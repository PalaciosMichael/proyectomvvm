﻿namespace DCOMProductions.MvvmDemo.ViewModels
{
    using System;
    using System.Diagnostics;
    using System.Windows.Input;
    using DCOMProductions.MvvmDemo.Commands;
    using DCOMProductions.MvvmDemo.Models;
    using DCOMProductions.MvvmDemo.Views;

    internal class ClienteViewModel
    {
        private cliente customer;
        private informacionCliente childViewModel;

        /// <summary>
        /// ingresamos una nueva instancia de clase "CustomerViewModel".
        /// </summary>
        public ClienteViewModel() {
            customer = new cliente("");
            childViewModel = new informacionCliente();
            UpdateCommand = new UpdateCustomerCommand(this);
        }

       
       
         // como no savbia que nombre ponerle lo  puse en ingles xd
        public cliente Customer {
            get {
                return customer;
            }
        }

        
        public ICommand UpdateCommand {
            get;
            private set;
        }

        /// <summary>
        /// guardamos la informacion que ingresaremos 
        /// </summary>
        public void SaveChanges() {
            ClientEInfoView view = new ClientEInfoView()
            {
                DataContext = childViewModel
            };

            childViewModel.Info = Customer.Name + " se actualizó en la base de datos";

            view.ShowDialog();
        }
    }
}
