﻿namespace DCOMProductions.MvvmDemo.Models
{
    using System;
    using System.ComponentModel;
    
    public class cliente : IDataErrorInfo, INotifyPropertyChanged
    {
        private string nombre;

        
        public cliente(String clientenombre) {
            nombre = clientenombre;
        }

        /// <summary>
        /// Obtiene o establece el nombre del cliente
        /// </summary>
        public String Name {
            get {
                return nombre;
            }
            set {
                nombre = value;
                OnPropertyChanged("Nombre");
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName) {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null) {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        public string Error
        {
            get;
            private set;
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "Name")
                {
                    if (String.IsNullOrWhiteSpace(Name))
                    {
                        Error = "El nombre no puede ser nulo ni estar vacío.";
                    }
                    else
                    {
                        Error = null;
                    }
                }

                return Error;
            }
        }

        #endregion
    }
}