﻿namespace DCOMProductions.MvvmDemo.Commands
{
    using System;
    using System.Windows.Input;
    using DCOMProductions.MvvmDemo.ViewModels;

    internal class UpdateCustomerCommand : ICommand
    {
        private ClienteViewModel viewModel;

        /// <summary>
        /// Inicializa una nueva instancia de la clase UpdateCustomerCommand.
        /// </summary>
        public UpdateCustomerCommand(ClienteViewModel viewModel) {
            this.viewModel = viewModel;
        }

        public event System.EventHandler CanExecuteChanged {
            add {
                CommandManager.RequerySuggested += value;
            }
            remove {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter) {
            return String.IsNullOrWhiteSpace(viewModel.Customer.Error);
        }

        public void Execute(object parameter) {
            viewModel.SaveChanges();
        }
    }
}